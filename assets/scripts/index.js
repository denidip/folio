$( document ).ready(function() {
    $("video").bind('playing',function() {
        $("video").css("z-index","inherit");
        $("#video-loader").attr("class", "devider");
    });
    $("#mobile-menu").click( function(){
        $("#mobile-menu").toggleClass("is-active");
        $("header nav").toggleClass("active");
    });
    $('#go').magnificPopup({
        type:'inline',
        mainClass: 'mfp-with-zoom',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
});