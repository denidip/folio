var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifycss = require('gulp-minify-css');
var jsmin = require('gulp-jsmin');

gulp.task('styles', function() {
    gulp.src([
        './assets/stylesheet/app.scss'
    ])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(autoprefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/css'));

    gulp.src([
        './assets/stylesheet/custom.scss'
    ])
        .pipe(concat('custom.css'))
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(autoprefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('scripts', function(){

    gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/magnific-popup/dist/jquery.magnific-popup.js'
    ])
        .pipe(concat('app.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));

    gulp.src('./assets/scripts/*.js')
        .pipe(concat('custom.js'))
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/js'));
});

gulp.task('watch', function() {
    gulp.watch('./assets/stylesheet/**/*.scss' , ['styles']);
    gulp.watch('./assets/scripts/*.js' , ['scripts']);
});

gulp.task('default', ['styles','scripts']);
